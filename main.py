#!/usr/bin/env python
# -*- coding: utf-8 -*-

#Script by: Paulo Jorge
#E-mail: paulo.jorge.pm@gmail.com

import urlparse
import lxml.html
import urllib
import time
import os

links_erros = []
log_prints = ""

def GetLinks(source_link):
    source_hostname = urlparse.urlparse(source_link).hostname
    source_scheme = urlparse.urlparse(source_link).scheme

    dom =  lxml.html.fromstring(urllib.urlopen(source_link).read())
    dom.make_links_absolute(source_scheme+'://'+source_hostname)
    
    links =  {} #lista final para o download, estrutura pasta:[links-list]
    folders = []
    
    for folder in dom.xpath("body/div[@id='page_content']/div/div[@class='folder']/div[@class='folder']/a"):
        folder_name = folder.text_content()
        folders.append(folder_name)

    for folder in folders:
        files = []
        for file in dom.xpath("body/div[@id='page_content']/div/div[@class='folder']/div[@class='folder'][a[text()='" + folder + "']]/div[@class='files']//a/@href"):
            file_name = file
            if file_name[0:5] == "http:":
                files.append(file_name.split("/")[-1])
            else:
                links_erros.append(folder + "/" + file_name)
        links[folder] = files
    
    return links

def download(folder, file_name):
    path = os.path.join(os.path.dirname(os.path.abspath(__file__)), folder, file_name)
    if not os.path.exists(os.path.dirname(path)):
        os.makedirs(os.path.dirname(path))
    link = "http://www.pssurvival.com/PS/" + folder + "/" + file_name
    if not os.path.exists(path):
        try:
            web_file = urllib.urlopen(link)
            local_file = open(path, 'wb')
            local_file.write(web_file.read())
            web_file.close()
            local_file.close()
        except:
            try:
                time.sleep(2) # espera alguns segundos e volta a tentar
                urllib.urlretrieve (link, path)
            except :
                try:
                    urllib.urlretrieve (link, path)
                except :
                    links_erros.append(folder + "/" + file_name)
                    print "----Download Error: " + folder + "/" + file_name
        print "++++Download Sucesso: " + folder + "/" + file_name
    else:
        print "!NOME DO FICHEIRO JÁ EXISTE NO DISCO, SKIP, VENHA OUTRO!"

#main_page = raw_input("Website?")
#main_page = 'http://www.pssurvival.com/'

index_page = 'http://www.pssurvival.com/index_complete.htm'

print "King Paulo presents: super hiper mega fixe programa para roubar ficheiros"
print "\r\n"
print "Gerando lista de ficheiros para download, a partir do link:"
print "http://www.pssurvival.com/index_complete.htm"
print "\r\n"
print "Aguardar alguns segundos..."

log_prints = log_prints + "\r\n" + "Gerando lista de ficheiros para download, a partir do link: http://www.pssurvival.com/index_complete.htm"

links = GetLinks(index_page)

for folder in links:
    print "Creando pasta: '" + folder + "'"
    log_prints = log_prints + "\r\n" + "Creando pasta: '" + folder + "'"
    list = links[folder]
    for link in list:
        print "Downloading do ficheiro: '" + folder + "/" + link + "'"
        log_prints = log_prints + "\r\n" + "Downloading do ficheiro: '" + link + "'"
        download(folder, link)

file = open("erros.txt", "w")
file.write( "\r\n".join(links_erros) )
file.close()

file = open("log.txt", "w")
file.write( log_prints )
file.close()

#json.dump(links, open("text.txt",'w'))

#file = open("links.txt", "w")
#file.write("\r\n".join(links))
#file.close()

#print links

#urllib.url_retrive("http://www.pssurvival.com/","")